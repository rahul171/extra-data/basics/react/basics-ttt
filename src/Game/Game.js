import React from 'react';
import './Game.css';
import Board from './Board/Board';

export default class Game extends React.Component {

    render() {
        return (
            <div className="game">
                <Board n={4} />
            </div>
        );
    }
}
