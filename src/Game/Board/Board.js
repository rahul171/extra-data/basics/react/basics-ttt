import React from 'react';
import './Board.css';
import Square from './Square/Square';
import produce from 'immer';

export default class Board extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            squares: this.getInitialStateSquares(this.props.n),
            isXNext: true,
            msg: 'start'
        }
    }

    getInitialStateSquares(n) {
        const squares = [];
        for (let i = 0; i < n; i++) {
            squares.push(Array(n).fill(null));
        }
        return squares;
    }

    render() {
        const msg = this.handleWinner() || this.state.msg;

        return (
            <div className="board">
                <div className="rows-list">
                    {this.getRows(this.props.n)}
                </div>
                <div className="msg">
                    {msg}
                </div>
            </div>
        );
    }

    handleWinner() {
        const winner = this.calculateWinner();
        if (winner) {
            return `${winner} has won!`;
        }
        const turnPlayer = this.state.isXNext ? 'X' : 'O';
        return `${turnPlayer}'s turn`;
    }

    getRows(n) {
        const elements = [];

        for (let i = 0; i < n; i++) {
            elements.push(
                <div className="row">
                    {this.getSquares(i, n)}
                </div>
            );
        }

        return elements;
    }

    getSquares(a, n) {
        const elements = [];

        let className = '';

        if (a === n - 1) {
            className += 'border-bottom ';
        }

        for (let i = 0; i < n; i++) {

            if (i === n - 1) {
                className += 'border-right ';
            }

            elements.push(
                <Square
                    className={className}
                    value={this.state.squares[a][i]}
                    onClick={() => { this.handleClick(a, i); }}
                />
            )
        }

        return elements;
    }

    handleClick(a, b) {
        if (this.state.squares[a][b] || this.calculateWinner()) {
            return;
        }

        const squares = produce(this.state.squares, (squares) => {
            squares[a][b] = this.state.isXNext ? 'X' : 'O';
        });
        const isXNext = !this.state.isXNext;

        this.setState({ squares, isXNext });
    }

    calculateWinner() {
        const squares = this.state.squares;

        for (let i = 0; i < squares.length; i++) {
            let equal = true;
            for (let j = 0; j < squares.length; j++) {
                if (!squares[i][0] || squares[i][0] !== squares[i][j]) {
                    equal = false;
                    break;
                }
            }
            if (equal) {
                return squares[i][0];
            }
        }

        for (let i = 0; i < squares.length; i++) {
            let equal = true;
            for (let j = 0; j < squares.length; j++) {
                if (!squares[0][i] || squares[0][i] !== squares[j][i]) {
                    equal = false;
                    break;
                }
            }
            if (equal) {
                return squares[i][0];
            }
        }

        let equal = true;

        for (let i = 0; i < squares.length; i++) {
            if (!squares[0][0] || squares[0][0] !== squares[i][i]) {
                equal = false;
                break;
            }
        }

        if (equal) {
            return squares[0][0];
        }

        return false;
    }
}
