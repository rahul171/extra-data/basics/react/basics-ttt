import React from 'react';
import './Square.css';

export default class Square extends React.Component {

    render() {
        return (
            <div
                className={`square ${this.props.className}`}
                onClick={this.props.onClick}
            >
                <span>{this.props.value}</span>
            </div>
        );
    }
}
